provider "google" {
    project = "wedding-planner-cjester"
    region = "us-east1"
    zone = "us-east1-b"
}

resource "google_compute_instance" "wedding-planner" {
    name = "wedding-planner-server"
    machine_type = "e2-small"

    boot_disk {
      initialize_params{
          image = "debian-cloud/debian-10"
      }
    }
    network_interface {
        network = "default"
        access_config {
          
        }
    }
}