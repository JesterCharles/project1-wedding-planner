# Wedding Planner

## Project Description

The Wedding Planner app is an online planner for a small wedding planner company. Employees to create and manage weddings. Employees can also send each other messages. It is a micro-services cloud deployed application. The application makes use of many GCP services including, Google Compute Engine, Datastore, Cloud SQL, Datastore, App Engine, Cloud Functions, Cloud Storage and Firebase Hosting.

## Technologies Used

* Node - version 14.17.6
* PostgreSQL
* React
* JavaScript/TypeScript
* Express
* App Engine 
* Cloud Functions
* Cloud Storage
* Firebase
* Datastore
* 

## Features

List of features ready and TODOs for future development
* SQL Database to store wedding information and expenses related to each wedding.
* Authorization service requiring a password match, generates a object that must be present to render certain aspects
* Messaging service that included a live chat feature, messages persisted on Datastore.
