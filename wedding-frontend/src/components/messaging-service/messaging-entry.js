import axios from "axios";
import React, { useState, useRef, useEffect } from "react";
import { useLocation } from "react-router";
import MessageTable from "./messaging-table";

export default function MessageEntry() {
    const urlFE = "https://wedding-planner-cjester.web.app";
    const log = useLocation();
    if (log.state === undefined) {
        if (!alert("Return to home page to login in order to access this information")) {
            window.location = `${urlFE}/home`;
        }
    }

    const date = new Date();
    const email = `${log.state.log.fname.toLowerCase()}@wed.com`;
    const [sender, setSender] = useState(email);
    const [receiver, setReceiver] = useState();
    const [messages, setMessages] = useState();
    const [message, setMessage] = useState();
    const [live, setLive] = useState(false);

    useEffect(() => {
        if (live === false) {
            return;
        } else {
            setTimeout(() => {
                liveChat();
            }, 2000);
        }
    });

    const midValue = useRef();

    function goLive() {
        setLive(true);
    }
    function goOffline() {
        setLive(false);
    }
    function setNewSender(event) {
        setSender(event.target.value);
    }
    function setNewReceiver(event) {
        setReceiver(event.target.value);
    }
    function setNewMessage(event) {
        setMessage(event.target.value);
    }

    async function allMessages() {
        const response = await axios.get("https://messaging-dot-wedding-planner-cjester.ue.r.appspot.com/messages");
        setMessages(response.data);
    }
    async function allSender() {
        const response = await axios.get(`https://messaging-dot-wedding-planner-cjester.ue.r.appspot.com/messages?sender=${sender}`);
        setMessages(response.data);
    }
    async function allReceiver() {
        const response = await axios.get(`https://messaging-dot-wedding-planner-cjester.ue.r.appspot.com/messages?reciever=${receiver}`);
        setMessages(response.data);
    }

    async function senderAndReceiver() {
        const response = await axios.get(
            `https://messaging-dot-wedding-planner-cjester.ue.r.appspot.com/messages?sender=${sender}&reciever=${receiver}`
        );
        setMessages(response.data);
    }
    async function liveChat() {
        if (live === true) {
            const response = await axios.get(
                `https://messaging-dot-wedding-planner-cjester.ue.r.appspot.com/messages?sender=${sender}&reciever=${receiver}`
            );
            const response2 = await axios.get(
                `https://messaging-dot-wedding-planner-cjester.ue.r.appspot.com/messages?sender=${receiver}&reciever=${sender}`
            );
            const first = response.data;
            const second = response2.data;
            const array = first.concat(second);
            setMessages(array);
        }
    }
    async function findMID() {
        const mid = midValue.current.value;
        const response = await axios.get(`https://messaging-dot-wedding-planner-cjester.ue.r.appspot.com/messages/${mid}`);
        setMessages(response.data);
    }

    async function submitMessage() {
        try {
            if (sender !== email) {
                alert(
                    `You are logged in as ${email}, but currently the sender is ${sender}. Please re-enter your email if you wish to send a message`
                );
                setMessages();
                return;
            }
            const messageObj = {
                message: message,
                sender: sender,
                reciever: receiver,
                timestamp: date.toISOString()
            };
            await axios.post("https://messaging-dot-wedding-planner-cjester.ue.r.appspot.com/messages", messageObj);
            setMessages();
            if (live === true) {
                const response = await axios.get(
                    `https://messaging-dot-wedding-planner-cjester.ue.r.appspot.com/messages?sender=${sender}&reciever=${receiver}`
                );
                setMessages(response.data);
            }
            alert("Message has been sent :D");
        } catch (error) {
            alert(`Either the ${sender} or ${receiver} does not exist in the database`);
        }
    }

    return (
        <div>
            <h1>Welcome to our Chat Page</h1>
            <h3>Feel free to talk to anyone in the batch! Just use their firstname@wed.com to communicate.</h3>
            <input type="text" value={sender} onChange={setNewSender}></input>
            <input type="text" placeholder="Enter Receiver Email" value={receiver} onChange={setNewReceiver}></input>
            <input type="text" placeholder="Enter Message ID" ref={midValue}></input>
            <button onClick={findMID}>Find Message By ID</button>
            <br></br>
            <button onClick={allMessages}>All Messages</button>
            <button onClick={allSender}>Sender's Messages</button>
            <button onClick={allReceiver}>Receiver's Messages</button>
            <button onClick={senderAndReceiver}>Messages between Sender and Receiver</button>
            <br></br>
            <textarea placeholder="Type message.." value={message} onChange={setNewMessage}></textarea>
            <button onClick={submitMessage}>Submit</button>
            <button onClick={goLive}>Live Chat</button>
            <button onClick={goOffline}>Offline</button>
            {messages === undefined ? <div></div> : <MessageTable mes={messages}></MessageTable>}
        </div>
    );
}
