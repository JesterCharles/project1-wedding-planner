export default function ImageShow(imgSrc) {
    const img = imgSrc.props;

    return (
        <>
            <a href={img}><img src={img}></img></a>
        </>
    );
}
