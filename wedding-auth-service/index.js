const { Datastore } = require("@google-cloud/datastore");
const cors = require("cors");
const express = require("express");

const app = express();
app.use(express.json());
app.use(cors());

const datastore = new Datastore();

app.get("/users", async (req, res) => {
    const query = datastore.createQuery("Users");
    const [data] = await datastore.runQuery(query);
    res.send(data);
});

app.get("/users/:email/verify", async (req, res) => {
    const key = datastore.key(["Users", req.params.email]);
    const associate = await datastore.get(key);
    if (associate[0] === undefined) {
        res.status(404);
        res.send(`Email is not contained within our database`);
    } else {
        res.status(200);
        res.send(true);
    }
});

app.patch("/users/login", async (req, res) => {
    const body = req.body;
    const query = datastore.createQuery("Users").filter("password", "=", body.password).filter("email", "=", body.email);
    const [data] = await datastore.runQuery(query);
    if (data[0] === undefined) {
        res.status(404);
        res.send(`The information provided does not exist in our database, please check email and password combination`);
    } else {
        const result = { fname: data[0].fname, lname: data[0].lname };
        res.send(result);
    }
});

app.put("/users/login", async (req, res) => {
    const body = req.body;
    const key = datastore.key(["Users", body.email]);
    await datastore.merge({ key: key, data: body });
    res.send("Update completed, try logging in with new information");
});

const PORT = process.env.PORT || 3002;
app.listen(PORT, () => console.log("App started"));
