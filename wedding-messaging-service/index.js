const { Datastore } = require("@google-cloud/datastore");
const express = require("express");
const cors = require("cors");
const axios = require("axios");

const app = express();
app.use(express.json());
app.use(cors());

const datastore = new Datastore();

const authURL = "https://wedding-planner-cjester.ue.r.appspot.com/users";

app.get("/messages", async (req, res) => {
	const sender = req.query.sender;
	const reciever = req.query.reciever;

	if (sender !== undefined && reciever !== undefined) {
		const query = datastore.createQuery("Messages").filter("sender", "=", sender).filter("reciever", "=", reciever);
		const [data] = await datastore.runQuery(query);
		res.send(data);
	} else if (sender !== undefined && reciever === undefined) {
		const query = datastore.createQuery("Messages").filter("sender", "=", sender);
		const [data] = await datastore.runQuery(query);
		res.send(data);
	} else if (sender === undefined && reciever !== undefined) {
		const query = datastore.createQuery("Messages").filter("reciever", "=", reciever);
		const [data] = await datastore.runQuery(query);
		res.send(data);
	} else {
		const query = datastore.createQuery("Messages");
		const [data] = await datastore.runQuery(query);
		res.send(data);
	}
});

app.get("/messages/:mid", async (req, res) => {
	const key = datastore.key(["Messages", Number(req.params.mid)]);
	const response = await datastore.get(key);
	res.send(response);
});

app.post("/messages", async (req, res) => {
	try {
		const body = req.body;
		const senderResponse = await axios.get(`${authURL}/${body.sender}/verify`);
		const recieverResponse = await axios.get(`${authURL}/${body.reciever}/verify`);

		if (senderResponse.data === recieverResponse.data) {
			const key = datastore.key("Messages");
			const datastoreObj = {
				message: body.message,
				sender: body.sender,
				reciever: body.reciever,
				timestamp: body.timestamp
			};

			const response = await datastore.save({ key: key, data: datastoreObj });
			res.status(201);
			res.send("New messages have been posted");
		}
	} catch (error) {
		res.status(404);
		res.send(`Check emails, one of them is not an authorized user`);
	}
});

const PORT = process.env.PORT || 3002;
app.listen(PORT, () => console.log("App started"));
